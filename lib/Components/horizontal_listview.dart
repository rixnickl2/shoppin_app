import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
             image_location: 'images/cats/desktop.png',
              imafe_caption: 'Desktops' ,
            ),

          Category(
            image_location: 'images/cats/Game.png',
            imafe_caption: 'Games' ,
          ),

          Category(
            image_location: 'images/cats/Media.png',
            imafe_caption: 'Media' ,
          ),

          Category(
            image_location: 'images/cats/Musics.png',
            imafe_caption: 'Musics' ,
          ),

          Category(
            image_location: 'images/cats/smartphone.png',
            imafe_caption: 'Mobiles' ,
          ),

          Category(
            image_location: 'images/cats/desktop.png',
            imafe_caption: 'Desktops' ,
          ),

          Category(
            image_location: 'images/cats/Game.png',
            imafe_caption: 'Games' ,
          ),

          Category(
            image_location: 'images/cats/Media.png',
            imafe_caption: 'Media' ,
          ),

          Category(
            image_location: 'images/cats/Musics.png',
            imafe_caption: 'Musics' ,
          ),

          Category(
            image_location: 'images/cats/smartphone.png',
            imafe_caption: 'Mobiles' ,
          ),

          Category(
            image_location: 'images/cats/desktop.png',
            imafe_caption: 'Desktops' ,
          ),

          Category(
            image_location: 'images/cats/Game.png',
            imafe_caption: 'Games' ,
          ),

          Category(
            image_location: 'images/cats/Media.png',
            imafe_caption: 'Media' ,
          ),

          Category(
            image_location: 'images/cats/Musics.png',
            imafe_caption: 'Musics' ,
          ),

          Category(
            image_location: 'images/cats/smartphone.png',
            imafe_caption: 'Mobiles' ,
          ),

        ],
      ),
    );
  }
}
class Category extends StatelessWidget {
  final String image_location;
  final String imafe_caption;

  Category({
    this.imafe_caption,
    this.image_location
});

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(2.0),
      child: InkWell(onTap: (){},
      child: Container(
        width: 100.0,
        child: ListTile(
          title: Image.asset(image_location,
          width: 80.0,
          height: 30.0,
          color: Colors.blue,),
          subtitle: Container(
            alignment: Alignment.topCenter,
            child: Text(imafe_caption),
          ),
        ),
      ),
      ),
    );
  }
}
