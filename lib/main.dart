import 'package:flutter/material.dart';

// My own import
import 'package:shoppin/Components/horizontal_listview.dart';
import 'package:shoppin/pages/products.dart';


void main() {
  runApp(new MaterialApp(
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget build(BuildContext context) {
  // ignore: unused_local_variable


    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.lightBlue,
        title: Text('Electronics ShoppIn'),
        actions: <Widget>[
          new IconButton(icon: Icon(Icons.search, color: Colors.white,), onPressed: () {}),
          new IconButton(icon: Icon(Icons.favorite, color: Colors.white,), onPressed: () {}),
          new IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white,), onPressed: () {})
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
             //header
             new UserAccountsDrawerHeader(accountName: Text('Loki STN'),
                accountEmail: Text('rixnickl2@gmail.com'),
                currentAccountPicture: GestureDetector(
                  child: new CircleAvatar(
                        backgroundColor: Colors.white,
                    child: Icon(Icons.person, color: Colors.blue),
               ),
             ),
                decoration: new BoxDecoration(
                    color: Colors.blue,
             ),
             ),
          //Body
          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ໜ້າຫລັກ'),
              leading: Icon(Icons.home, color: Colors.blue,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ສິນຄ້າ'),
              leading: Icon(Icons.computer, color: Colors.blue,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ປະເພດສິນຄ້າ'),
              leading: Icon(Icons.category, color: Colors.blue,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ສັ່ງຊື້'),
              leading: Icon(Icons.assignment, color: Colors.blue,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ຂໍ້ມູນສວນຕົວ'),
              leading: Icon(Icons.person_pin_circle, color: Colors.blue,),
            ),
          ),

          Divider(),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ຕັ້ງຄ່າ'),
              leading: Icon(Icons.settings, color: Colors.grey,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ກ່ຽວກັບພວກເຮົາ'),
              leading: Icon(Icons.help, color: Colors.grey,),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text('ອອກຈາກລະບົບ'),
              leading: Icon(Icons.exit_to_app, color: Colors.grey,),
            ),
          ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          //Image Carousel begin from her
          //imageCarousel,
          //padding widget
          new Padding(padding: const EdgeInsets.all(8.0),
            child: new Text('ປະເພດສິນຄ້າ'),),


          //*************Horizontal list view begin from here */
            HorizontalList(),

          //padding widget
          new Padding(padding: const EdgeInsets.all(13.0),
            child: new Text('ສິນຄ້າປັດຈຸບັນ'),),


          // Grid View
          Container(
           height: 320.0,
            child: Products(),
          ),

          new Padding(padding: const EdgeInsets.all(13.0),
            child: new Text('ສີ້ນຄ່າຍອດນິຍົມ'),),

          Container(
            height: 320.0,
            child: Products(),
          ),

          new Padding(padding: const EdgeInsets.all(13.0),
            child: new Text('ສີ້ນຄ່າຂ່າຍດີປະຈຳອາທິດ'),),

          Container(
            height: 320.0,
            child: Products(),
          ),

        ],
      ),
    );
  }
}
