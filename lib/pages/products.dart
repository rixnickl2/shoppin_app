import 'package:flutter/material.dart';
import 'package:shoppin/pages/product_detail.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {

  // ignore: non_constant_identifier_names
  var Products_list = [
    {
      "name": "Loki",
      "picture": "images/Products/adv_1.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
    {
      "name": "Loki",
      "picture": "images/Products/adv_2.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
    {
      "name": "Loki",
      "picture": "images/Products/adv_3.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
    {
      "name": "Loki",
      "picture": "images/Products/best_1.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
    {
      "name": "Loki",
      "picture": "images/Products/best_2.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
    {
      "name": "Loki",
      "picture": "images/Products/best_3.png",
      "Qty": "2",
      "price": "210",
      "total_price": "420",
    },
  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: Products_list.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2
        ),
        itemBuilder: (BuildContext context, int index){
          return Single_prod(
            product_name: Products_list[index]['name'],
            product_picture: Products_list[index]['picture'],
            product_qty: Products_list[index]['Qty'],
            product_price: Products_list[index]['price'],
            product_total_price: Products_list[index]['total_price'],

          );
        });
  }
}

class Single_prod extends StatelessWidget {
  final product_name;
  final product_picture;
  final product_qty;
  final product_price;
  final product_total_price;

  Single_prod({
  this.product_name,
  this.product_picture,
  this.product_qty,
  this.product_price,
  this.product_total_price
});
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: product_name,
        child: Material(
          child: InkWell(onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new ProductDetail())),
           child: GridTile(
            footer: Container(
              color: Colors.white,
              child: ListTile(
                leading: Text(product_name, style: TextStyle(fontWeight: FontWeight.bold),),
                title: Text('\$$product_price', style: TextStyle(color: Colors.blue),),
              ),
            ),
            child: Image.asset(product_picture, fit: BoxFit.cover,)),
          ),
        ),
      ),
    );
  }
}


